package com.bucares.kpalindrome.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bucares.kpalindrome.dao.KpalindromeDAO;

@Service
public class KpalindromeImp implements Kpalindrome {

	@Autowired
	private KpalindromeDAO kpalindromeDAO;
	
	
	// find if given string is 
    // K-Palindrome or not 
    static int isKPalDP(String str1,  String str2, int m, int n)  
    {
          
        // Create a table to store  
        // results of subproblems 
        int dp[][] = new int[m + 1][n + 1]; 
  
        // Fill dp[][] in bottom up manner 
        for (int i = 0; i <= m; i++)  
        { 
            for (int j = 0; j <= n; j++)  
            { 
                  
                // If first string is empty, 
                // only option is to remove all 
                // characters of second string 
                if (i == 0)  
                { 
                    // Min. operations = j 
                    dp[i][j] = j;  
                }  
                  
                // If second string is empty,  
                // only option is to remove all 
                // characters of first string 
                else if (j == 0)  
                { 
                    // Min. operations = i 
                    dp[i][j] = i;  
                }  
                  
                // If last characters are same,  
                // ignore last character and 
                // recur for remaining string 
                else if (str1.charAt(i - 1) == 
                        str2.charAt(j - 1))  
                { 
                    dp[i][j] = dp[i - 1][j - 1]; 
                }  
                  
                // If last character are different, 
                //  remove it and find minimum 
                else 
                { 
                    // Remove from str1 
                    // Remove from str2 
                    dp[i][j] = 1 + Math.min(dp[i - 1][j],  
                            dp[i][j - 1]);  
                } 
            } 
        } 
        return dp[m][n]; 
    }
	
	@Override
	public boolean isKpalindrome(String word, int k) {
		
		String revStr = word; 
	        revStr = reverse(revStr); 
	        int len = word.length(); 
	  
	        return (isKPalDP(word, revStr, len, len) <= k * 2);
	}
	

	static String reverse(String str) 
    { 
        StringBuilder sb = new StringBuilder(str); 
        sb.reverse(); 
        return sb.toString(); 
    }
	

	@Override
	public boolean isPalindrome(String word) {
		
        int i = 0, j = word.length() - 1; 
  
        while (i < j) { 
  
            if (word.charAt(i) != word.charAt(j)) 
                return false; 
            i++; 
            j--; 
        }

        return true;  
	    	
	}


}
