package com.bucares.kpalindrome.service;

public interface Kpalindrome {
	
    /**
     * Returns true if it can be transformed into a palindrome by removing
     * any amount of characters from 0 to k.
     *
     * @param  word the string to be evaluated
     * @param  k the k level to calculate a palindrome
     * @return      true/false whether the word is a k-palindrome or not
     */
    boolean isKpalindrome(String word, int k);


    /**
     * Returns true if the word is a palindrome
     *
     * @param  word the string to be evaluated
     * @return  true/false whether the word is a palindrome or not
     */
    boolean isPalindrome(String word);
    
}
