package com.bucares.kpalindrome.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="History")
public class History {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="word")
	private String word;
	
	@Column(name="k")
	private int k;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	
	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}
	
	public History() {}
	
	public History(int id, String word, int k) {
		this.id = id;
		this.word = word;
		this.k = k;
	}
	
	@Override
	public String toString() {
		return " [id=" + id + ", word=" + word + ", k=" + k + "]";
	}
}
