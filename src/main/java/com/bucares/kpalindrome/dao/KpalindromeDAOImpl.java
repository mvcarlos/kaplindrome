package com.bucares.kpalindrome.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;

import com.bucares.kpalindrome.entity.History;

@Repository
public class KpalindromeDAOImpl implements KpalindromeDAO {

	//Conect to DB
	EntityManagerFactory efact = Persistence.createEntityManagerFactory("ka-pu");
    EntityManager eman = efact.createEntityManager();

	@Override
	public Long getHistoryByWordAndK(String word, int k) {
		
			//QUery find previous data
            Query query = eman.createQuery(
                    "SELECT count(h) FROM History h WHERE h.word = :word and h.k = :k");
                query.setParameter("word", word);
                query.setParameter("k", k);
            Long rowCnt = (Long)  query.getSingleResult();
            
            //If no results save the data
            if(rowCnt.intValue() == 0) {
            	
                eman.getTransaction().begin();

                History kp = new History();
                kp.setWord(word);
                kp.setK(k);
                //Save
                eman.persist(kp);
                eman.getTransaction().commit();
                   
            }
            
            return rowCnt;
            
	}

}
