package com.bucares.kpalindrome.dao;


public interface KpalindromeDAO {
	
	
	/**
	 * Check if data is in DB, if no exist save for history 
     * Returns 0 if data no exist
     *
     * @param  word the string to be evaluated
     * @param  k the k level to calculate a palindrome
     * @return     Long
     */
	public Long getHistoryByWordAndK(String word, int k);

}
