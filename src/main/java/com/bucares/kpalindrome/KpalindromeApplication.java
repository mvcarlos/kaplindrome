package com.bucares.kpalindrome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KpalindromeApplication {

	public static void main(String[] args) {
		SpringApplication.run(KpalindromeApplication.class, args);
	}

}
