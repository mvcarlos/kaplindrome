package com.bucares.kpalindrome.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bucares.kpalindrome.service.Kpalindrome;
import com.bucares.kpalindrome.dao.*;
import org.json.*;
import com.bucares.kpalindrome.entity.*;

@RestController
@RequestMapping("/api") //http://127.0.0.1:8080/api/
public class KpalindromeRestController {
	
		@Autowired
		private Kpalindrome kpalindromeService;
		
		@Autowired
		private KpalindromeDAO kpalindromeDAO;

		/*http://127.0.0.1:8080/api/kpalindrome*/
		@CrossOrigin(origins = "*", methods= {RequestMethod.POST})
		@RequestMapping(value="/kpalindrome", 
        method=RequestMethod.POST, 
        produces=MediaType.APPLICATION_JSON_VALUE)
		//public String isKPalindrome(@RequestParam String s,@RequestParam int k){
		public String isKPalindrome(@RequestBody History formData){
			
			String s = formData.getWord();
			int k = formData.getK();
			
			//Check data db
			Long history = kpalindromeDAO.getHistoryByWordAndK(s, k);
			
			boolean cached = false;
			
			if(history.intValue() >= 1)
				cached = true;
			
			boolean isKpalindrome =  kpalindromeService.isKpalindrome(s, k);
			  
			 JSONObject result = new JSONObject();

			 result.put("answer", isKpalindrome);
			 result.put("cached", cached);
		        
			return result.toString();
			//Return JSON result
			
		}
		
		@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
		@RequestMapping(value="/test", 
        method=RequestMethod.GET, 
        produces=MediaType.APPLICATION_JSON_VALUE)
		public String testMessage(){
			
			  
			 JSONObject result = new JSONObject();

			 result.put("message", "test");
		        
			return result.toString();
			//Return JSON result
			
		}
		
}
